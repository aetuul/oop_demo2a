import java.util.ArrayList;

/**
 * Luokka mallintaa muistivihkoa
 */
public class Muistivihko {
    private String nimi;
    private String omistaja;
    private ArrayList<String> muistiinpanot;

    /**
     *
     * @param nimi     on merkkijono, joka kertoo muistivihkon nimen
     * @param omistaja on merkkijono, joka kertoo muistivihkon omistajan nimen
     *                 Luokan konstruktori, jossa parametreina nimi ja omistaja
     *                 Konstruktori asettaa olion nimen, omistajan ja muistiinpanot
     */
    public Muistivihko(String nimi, String omistaja) {
        this.nimi = nimi;
        this.omistaja = omistaja;
        muistiinpanot = new ArrayList<>();
    }

    /**
     *
     * @return Muistikirjan nimi
     */
    public String getNimi() {
        return nimi;
    }

    /**
     *
     * @param nimi uusi nimi muistivihkolle
     *             Metodi asettaa muistivihkolle uuden nimen
     */
    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    /**
     *
     * @return Muistivihko omistaja
     */
    public String getOmistaja() {
        return omistaja;
    }

    /**
     *
     * @param omistaja uusi omistaja muistivihkolle
     *                 Asettaa uuden omistajan muistivihkolle
     */
    public void setOmistaja(String omistaja) {
        this.omistaja = omistaja;
    }

    /**
     *
     * @param viesti lisättävän muistiinpanon sisältö
     */
    public void lisaaMuistiinpano(String viesti) {
        this.muistiinpanot.add(viesti);
    }

    /**
     *
     * @return Muistiinpanojen lukumäärä
     */
    public int muistiinpanoja() {
        return muistiinpanot.size();
    }

    /**
     * Tulostaa kaikki muistiinpanot
     */
    public void tulostaMuistiinpanot() {
        for (String viesti : muistiinpanot) {
            System.out.println(viesti);
        }
    }
}